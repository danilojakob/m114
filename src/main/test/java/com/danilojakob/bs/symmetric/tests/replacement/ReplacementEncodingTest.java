package com.danilojakob.bs.symmetric.tests.replacement;

import com.danilojakob.bs.util.helper.EncryptionHelper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ReplacementEncodingTest {

    private EncryptionHelper encryptionHelper_;

    @Before
    public void createEncryptionHelperObject() {
        encryptionHelper_ = new EncryptionHelper();
    }

    @Test
    public void testReplacementEncoding() {
        String encryptedText = encryptionHelper_.replacementEncryption("Micky Maus", "M", "T");
        Assert.assertEquals("Ticky Taus", encryptedText);
    }

    @After
    public void deleteEncryptionHelperObject() {
        encryptionHelper_ = null;
    }
}
