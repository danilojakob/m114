package com.danilojakob.bs.asymmetric.tests.rsa;

import com.danilojakob.M114Constants;
import com.danilojakob.bs.util.helper.EncryptionHelper;
import com.danilojakob.bs.util.helper.KeyGenerator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.PublicKey;

public class EncryptFileTest {

    private EncryptionHelper encryptionHelper_;
    private KeyGenerator keyGenerator_;
    private PublicKey publicKey_;

    private final Logger LOG = LoggerFactory.getLogger(EncryptFileTest.class);
    @Before
    public void setUp() {
        encryptionHelper_ = new EncryptionHelper();
        try {
            keyGenerator_ = new KeyGenerator(2048);
            keyGenerator_.generateKeys();
            publicKey_ = keyGenerator_.getPublicKey();
            File testFile = new File(M114Constants.WORKING_DIR + "danilo.txt");
            FileOutputStream stream = new FileOutputStream(testFile);
            stream.write("Text123".getBytes());
            stream.close();
            LOG.info("Generated keys and created the test file");
        } catch (Exception ex) {
            /* POSSIBLE EXCEPTIONS */
            LOG.error("Error while generating keys and creating the test File: {}", ex.toString());
        }
    }

    @Test
    public void encryptTest() {
        try {
            File file = new File(M114Constants.WORKING_DIR + "danilo.txt");
            encryptionHelper_.encryptFile(file, publicKey_);
            Assert.assertTrue(file.exists());
            Assert.assertNotNull(file);
            LOG.info("Encrypted the file");
        } catch (Exception ex) {
            /* POSSIBLE EXCEPTION */
            LOG.error("Error while encrypting the file: {}", ex.toString());
        }
    }

    @After
    public void deleteObjects() {
        encryptionHelper_ = null;
        keyGenerator_ = null;
        publicKey_ = null;
        try {
            Files.delete(Paths.get(M114Constants.WORKING_DIR + "danilo.txt"));
            LOG.info("Deleted the test file");
        } catch (IOException ex) {
            /* POSSIBLE EXCEPTION */
            LOG.error("Error while deleting the encrypted file: {}", ex.toString());
        }
    }
}
