package com.danilojakob.bs.asymmetric.tests.rsa;

import com.danilojakob.bs.util.helper.KeyGenerator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

public class SaveKeyPairTest {

    private KeyGenerator keyGenerator_;

    @Before
    public void generateKeys() throws IOException, NoSuchAlgorithmException, NoSuchProviderException {
        keyGenerator_ = new KeyGenerator(1024);
        keyGenerator_.generateKeys();
        keyGenerator_.saveKeys("keyPairs/privateKey", keyGenerator_.getPrivateKey().getEncoded());
        keyGenerator_.saveKeys("keyPairs/publicKey", keyGenerator_.getPublicKey().getEncoded());
    }

    @Test
    public void saveKeyPairTest() {
        File privateKey = new File("keyPairs/privateKey");
        File publicKey = new File("keyPairs/publicKey");

        Assert.assertTrue(privateKey.exists());
        Assert.assertTrue(publicKey.exists());

    }

    @After
    public void deleteKeyGeneratorObject() throws IOException{
        keyGenerator_ = null;
        Files.delete(Paths.get("keyPairs/privateKey"));
        Files.delete(Paths.get("keyPairs/publicKey"));
        Files.delete(Paths.get("keyPairs"));
    }
}
