package com.danilojakob.bs.asymmetric.tests.rsa;

import com.danilojakob.bs.util.helper.EncryptionHelper;
import com.danilojakob.bs.util.helper.KeyGenerator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.security.PublicKey;

public class EncryptTextTest {

    private EncryptionHelper encryptionHelper_;
    private KeyGenerator keyGenerator_;
    private PublicKey publicKey_;

    @Before
    public void setUp() {
        encryptionHelper_ = new EncryptionHelper();
        try {
            keyGenerator_ = new KeyGenerator(2048);
            keyGenerator_.generateKeys();
            publicKey_ = keyGenerator_.getPublicKey();
        } catch (Exception ex) {

        }
    }

    @Test
    public void encryptText() {
        try {
            String encryptedText = encryptionHelper_.encryptText("Nice Sache mit dem Key", publicKey_);
            Assert.assertEquals("Bn3lhysb1I7bdtp+MrqLD2nCZI6f5GUAwPYTRBUYq9m4z0GRUkSm7T3c8JeM9kPUbMzQGQvhi6NWnhYWqFgYz1zGz5LHS2IwducCrxAqJZofonEr7/w5JSVOX74lIhX1Vwbd45c44RmoeOYQWyD+HJa9sF41thJJS36cFBNiHBVzutCk0mv3o5f7C4NYOIG+TBdIX3yjs2Bqb+CK5Ttv6QleWbDU8lVL4+0gvL1fmbPsJqX7oQulItCGDzrYeHmfjZ6IY2gbSRcZJ1ibuR9AXnkHsLL63dXYrKK5OBzxxQX/7/4mARAO4jmoaAJGefhc9ZO9CQRIXVnq6xTh1mmd9Q==", encryptedText);
        } catch (Exception ex) {

        }
    }

    @After
    public void deleteObjects() {
        encryptionHelper_ = null;
        keyGenerator_ = null;
        publicKey_ = null;
    }
}
