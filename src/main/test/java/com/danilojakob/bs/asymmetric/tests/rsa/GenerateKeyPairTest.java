package com.danilojakob.bs.asymmetric.tests.rsa;

import com.danilojakob.bs.util.helper.KeyGenerator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

public class GenerateKeyPairTest {

    private KeyGenerator keyGenerator_;

    @Before
    public void createKeyGeneratorObject() throws NoSuchAlgorithmException, NoSuchProviderException {
        keyGenerator_ = new KeyGenerator(1024);
    }

    @Test
    public void generateKeyPairTest() {
        keyGenerator_.generateKeys();
        Assert.assertNotNull(keyGenerator_.getPrivateKey());
        Assert.assertNotNull(keyGenerator_.getPublicKey());

        Assert.assertTrue(keyGenerator_.getPublicKey().toString().length() > 0);
        Assert.assertTrue(keyGenerator_.getPrivateKey().toString().length() > 0);
    }

    @After
    public void deleteKeyGeneratorObject() {
        keyGenerator_ = null;
    }
}
