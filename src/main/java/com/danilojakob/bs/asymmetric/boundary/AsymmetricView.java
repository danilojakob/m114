package com.danilojakob.bs.asymmetric.boundary;

import com.danilojakob.M114Constants;
import com.danilojakob.bs.util.helper.DownloadHelper;
import com.github.appreciated.app.layout.annotations.MenuIcon;
import com.vaadin.cdi.CDIView;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.simplefiledownloader.SimpleFileDownloader;

@CDIView(M114Constants.CDIVIEW_ASYMMETRIC)
@MenuIcon(VaadinIcons.SAFE)
public class AsymmetricView extends VerticalLayout implements View {


    private Button downloadButton_;
    private SimpleFileDownloader downloader_;
    private DownloadHelper downloadHelper_;

    public AsymmetricView() {
        downloader_ = new SimpleFileDownloader();
        downloadHelper_ = new DownloadHelper();
        downloadButton_ = new Button("Download");
        downloadButton_.addClickListener(this::download);
        addComponent(downloadButton_);
        addExtension(downloader_);
    }

    private void download(Button.ClickEvent event) {
        try {
            StreamResource streamResource = downloadHelper_.createStreamResource(M114Constants.WORKING_DIR + "keyPairs\\publicKey", "test.pem");
            downloader_.setFileDownloadResource(streamResource);
            downloader_.download();
        } catch (Exception ex) {
            System.err.println(ex);
        }
    }
}
