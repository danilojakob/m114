package com.danilojakob.bs.asymmetric.tests.rsa;

import com.danilojakob.bs.util.helper.EncryptionHelper;
import com.danilojakob.bs.util.helper.KeyGenerator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.security.PrivateKey;
import java.security.PublicKey;

public class DecryptTextTest {

    private EncryptionHelper encryptionHelper_;
    private KeyGenerator keyGenerator_;
    private PrivateKey privateKey_;
    private PublicKey publicKey_;
    private String encryptedText_;

    @Before
    public void setUp() {
        encryptionHelper_ = new EncryptionHelper();
        try {
            keyGenerator_ = new KeyGenerator(2048);
            keyGenerator_.generateKeys();
            privateKey_ = keyGenerator_.getPrivateKey();
            publicKey_ = keyGenerator_.getPublicKey();
            encryptedText_ = encryptionHelper_.encryptText("Nice Sache mit dem Key", publicKey_);
        } catch (Exception ex) {

        }
    }

    @Test
    public void decryptText() {
        try {
            String decryptedText = encryptionHelper_.decryptText(encryptedText_, privateKey_);
            Assert.assertEquals("Nice Sache mit dem Key", decryptedText);
        } catch (Exception ex) {

        }
    }

    @Before
    public void deleteObjects() {
        encryptionHelper_ = null;
        keyGenerator_ = null;
        privateKey_ = null;
    }
}
