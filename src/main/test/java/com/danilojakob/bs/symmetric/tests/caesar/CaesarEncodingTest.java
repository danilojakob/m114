package com.danilojakob.bs.symmetric.tests.caesar;

import com.danilojakob.bs.util.helper.EncryptionHelper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Class for testing the Caesar encoding
 */
public class CaesarEncodingTest {
    private EncryptionHelper encryptionHelper_;

    @Before
    public void createEncryptionHelperObject() {
        encryptionHelper_ = new EncryptionHelper();
    }

    @Test
    public void testCaesarEncoding() {
        String encryptedText = encryptionHelper_.caesarEncryption("Danilo", true);
        Assert.assertEquals("Hermps", encryptedText);
    }

    @After
    public void deleteEncryptionHelperObject() {
        encryptionHelper_ = null;

    }
}
