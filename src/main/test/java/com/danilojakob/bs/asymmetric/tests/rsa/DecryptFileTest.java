package com.danilojakob.bs.asymmetric.tests.rsa;

import com.danilojakob.M114Constants;
import com.danilojakob.bs.util.helper.EncryptionHelper;
import com.danilojakob.bs.util.helper.KeyGenerator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.PrivateKey;
import java.security.PublicKey;

public class DecryptFileTest {

    private KeyGenerator keyGenerator_;
    private PrivateKey privateKey_;
    private PublicKey publicKey_;
    private EncryptionHelper encryptionHelper_;

    @Before
    public void setUp() {
        try {
            keyGenerator_ = new KeyGenerator(2048);
            keyGenerator_.generateKeys();
            privateKey_ = keyGenerator_.getPrivateKey();
            publicKey_ = keyGenerator_.getPublicKey();
            encryptionHelper_ = new EncryptionHelper();
            //Generate the file and encrypt it
            File testFile = new File(M114Constants.WORKING_DIR + "danilo.txt");
            FileOutputStream stream = new FileOutputStream(testFile);
            stream.write("This is an example".getBytes());
            stream.close();
            encryptionHelper_.encryptFile(testFile, publicKey_);
        } catch (Exception ex) {
            /* POSSIBLE EXCEPTIONS */
        }
    }

    @Test
    public void decryptFileTest() {
        File file = new File(M114Constants.WORKING_DIR + "danilo.txt");
        encryptionHelper_.decryptFile(file, privateKey_);
        try {
            FileInputStream stream = new FileInputStream(file);
            byte[] bytes = new byte[(int) file.length()];
            stream.read(bytes);
            //TODO: Method works, need to convert bytes to String
            String text = new String(bytes);
            Assert.assertEquals("This is an example", text);
        } catch (Exception ex) {
            /* POSSIBLE EXCEPTIONS */
        }
    }

    @After
    public void deleteObjects() {
        encryptionHelper_ = null;
        keyGenerator_ = null;
        privateKey_ = null;
        publicKey_ = null;
        try {
            Files.delete(Paths.get("danilo.txt"));
        } catch (Exception ex) {
            /* POSSIBLE EXCEPTIONS */
        }
    }
}
