package com.danilojakob.bs.symmetric;

import com.danilojakob.bs.symmetric.tests.HashTest;
import com.danilojakob.bs.symmetric.tests.caesar.CaesarDecodingTest;
import com.danilojakob.bs.symmetric.tests.caesar.CaesarEncodingTest;
import com.danilojakob.bs.symmetric.tests.replacement.ReplacementEncodingTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

//Make a Test Suite out of the class
@RunWith(Suite.class)

//Define the classes that need to be run
@Suite.SuiteClasses({
        CaesarDecodingTest.class,
        CaesarEncodingTest.class,
        ReplacementEncodingTest.class,
        HashTest.class
})

public class SymmetricTest {
    /* Class is just a placeholder */
}
