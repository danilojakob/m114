package com.danilojakob.bs.symmetric.tests;

import com.danilojakob.bs.util.helper.EncryptionHelper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HashTest {

    private EncryptionHelper encryptionHelper_;

    @Before
    public void createEncryptionHelperObject() {
        encryptionHelper_ = new EncryptionHelper();
    }

    @Test
    public void hashTest() {
        String text = "Danilo";
        String hashedText = encryptionHelper_.hash(text);
        Assert.assertEquals("2ac52d82f1c633c615ba018b8dcfc6458d7769a5e323a0d1e1d2041755520a34", hashedText);
    }

    @After
    public void deleteObjects() {
        encryptionHelper_ = null;
    }
}
