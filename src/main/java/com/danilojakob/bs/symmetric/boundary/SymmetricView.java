package com.danilojakob.bs.symmetric.boundary;

import com.danilojakob.M114Constants;
import com.danilojakob.bs.util.helper.EncryptionHelper;
import com.github.appreciated.app.layout.annotations.MenuIcon;
import com.jarektoro.responsivelayout.ResponsiveColumn;
import com.jarektoro.responsivelayout.ResponsiveLayout;
import com.jarektoro.responsivelayout.ResponsiveRow;
import com.vaadin.cdi.CDIView;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.server.Responsive;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

@CDIView(M114Constants.CDIVIEW_SYMMETRIC)
@MenuIcon(VaadinIcons.SAFE_LOCK)
public class SymmetricView extends VerticalLayout implements View {
    //Caesar
    private Label viewTitle_;
    private ResponsiveLayout layout_;
    private TextField caesarTfToEncypt_;
    private Label caesarLabelEncryptedText_;
    private Button caesarEncryptButton_;
    private Label caesarDescription_;
    private Label caesarTitle_;
    private VerticalLayout textLayoutCaesar_;
    private CheckBox chkEncodeDecode_;
    //Replacement
    private VerticalLayout textLayoutReplacement_;
    private Label replacementTitle_;
    private Label replacementDescription_;
    private TextField replacementTfToEncrypt_;
    private Label replacementLabelEncryptedText_;
    private Button replacementEncryptButton_;
    private TextField replacementCharacterToReplace_;
    private TextField replacementCharacterReplacement_;

    private EncryptionHelper encryptionHelper_;

    public SymmetricView() {
        setMargin(true);
        setSpacing(true);

        //Caesar
        chkEncodeDecode_ = new CheckBox(M114Constants.CHECKBOX_CAPTION);
        viewTitle_ = new Label(M114Constants.MENU_CAPTION_SYMMETRIC);
        viewTitle_.addStyleName(ValoTheme.LABEL_H1);
        layout_ = new ResponsiveLayout();
        caesarTfToEncypt_ = new TextField();
        caesarLabelEncryptedText_ = new Label(M114Constants.EMPTY_STRING);
        caesarTfToEncypt_.setPlaceholder("Text...");
        caesarEncryptButton_ = new Button(M114Constants.BUTTON_ENCRYPT_CAPTION);
        caesarEncryptButton_.addClickListener(this::caesarEncryptText);
        caesarDescription_ = new Label(M114Constants.CAESAR_DESCRIPTION);
        caesarDescription_.setStyleName(ValoTheme.LABEL_H3);
        caesarDescription_.setStyleName(ValoTheme.LAYOUT_HORIZONTAL_WRAPPING);
        caesarDescription_.setId("margin-bottom");
        caesarDescription_.setWidth("100%");
        caesarTitle_ = new Label(M114Constants.CAESAR_TITLE);
        caesarTitle_.setStyleName(ValoTheme.LABEL_H2);
        caesarTitle_.setStyleName(ValoTheme.LAYOUT_HORIZONTAL_WRAPPING);
        caesarTitle_.setWidth("100%");
        textLayoutCaesar_ = new VerticalLayout();
        textLayoutCaesar_.setMargin(false);
        textLayoutCaesar_.addComponents(viewTitle_, caesarTitle_, caesarDescription_);
        textLayoutCaesar_.setComponentAlignment(caesarDescription_, Alignment.MIDDLE_CENTER);
        Responsive.makeResponsive(caesarDescription_, viewTitle_);

        //Replacement Row
        textLayoutReplacement_ = new VerticalLayout();
        textLayoutReplacement_.setMargin(false);
        replacementTitle_ = new Label(M114Constants.REPLACEMENT_TITLE);
        replacementTitle_.setStyleName(ValoTheme.LABEL_H2);
        replacementTitle_.setStyleName(ValoTheme.LAYOUT_HORIZONTAL_WRAPPING);
        replacementTitle_.setWidth("100%");
        replacementDescription_ = new Label(M114Constants.REPLACEMENT_DESCRIPTION);
        replacementDescription_.setStyleName(ValoTheme.LABEL_H3);
        replacementDescription_.setStyleName(ValoTheme.LAYOUT_HORIZONTAL_WRAPPING);
        replacementDescription_.setId("margin-bottom");
        replacementDescription_.setWidth("100%");
        textLayoutReplacement_.addComponents(replacementTitle_, replacementDescription_);
        replacementTfToEncrypt_ = new TextField();
        replacementTfToEncrypt_.setPlaceholder(M114Constants.TF_TO_ENCRYPT_CAPTION);
        replacementLabelEncryptedText_ = new Label();
        replacementEncryptButton_ = new Button(M114Constants.BUTTON_ENCRYPT_CAPTION);
        replacementEncryptButton_.addClickListener(this::replacementEncryptText);
        replacementCharacterToReplace_ = new TextField();
        replacementCharacterToReplace_.setPlaceholder(M114Constants.TF_TO_REPLACE_CAPTION);
        replacementCharacterReplacement_ = new TextField();
        replacementCharacterReplacement_.setPlaceholder(M114Constants.TF_REPLACEMENT_CAPTION);
        Responsive.makeResponsive(replacementTitle_, replacementDescription_);
        //
        encryptionHelper_ = new EncryptionHelper();
        //Create Responsive Rows and Columns
        //Rows
        ResponsiveRow responsiveRowCaesarEncryption = new ResponsiveRow();
        responsiveRowCaesarEncryption.setHorizontalSpacing(false);
        responsiveRowCaesarEncryption.setVerticalSpacing(true);
        /* Caesar */
        //Columns
        ResponsiveColumn responsiveColumnOne = new ResponsiveColumn(12,4,3,2);
        ResponsiveColumn responsiveColumnTwo = new ResponsiveColumn(12,4,3,2);
        ResponsiveColumn responsiveColumnThree = new ResponsiveColumn(12,4,3,2);
        ResponsiveColumn responsiveColumnFour = new ResponsiveColumn(12,4,3,2);
        //Set components for the rows
        responsiveColumnOne.withComponent(caesarTfToEncypt_);
        responsiveColumnTwo.withComponent(chkEncodeDecode_);
        responsiveColumnTwo.setAlignment(ResponsiveColumn.ColumnComponentAlignment.LEFT);
        responsiveColumnThree.withComponent(caesarEncryptButton_);
        responsiveColumnThree.setAlignment(ResponsiveColumn.ColumnComponentAlignment.LEFT);
        responsiveColumnFour.withComponent(caesarLabelEncryptedText_);
        responsiveColumnFour.setAlignment(ResponsiveColumn.ColumnComponentAlignment.LEFT);
        //Set Columns of responsiveRowCaesarEncryption
        responsiveRowCaesarEncryption.addColumn(responsiveColumnOne);
        responsiveRowCaesarEncryption.addColumn(responsiveColumnTwo);
        responsiveRowCaesarEncryption.addColumn(responsiveColumnThree);
        responsiveRowCaesarEncryption.addColumn(responsiveColumnFour);
        /* Replacement */
        ResponsiveRow responsiveRowReplacementEncryption = new ResponsiveRow();
        responsiveRowReplacementEncryption.setHorizontalSpacing(false);
        responsiveRowReplacementEncryption.setVerticalSpacing(true);
        //Columns
        ResponsiveColumn responsiveColumnFive = new ResponsiveColumn(12,4,3,2);
        ResponsiveColumn responsiveColumnSix = new ResponsiveColumn(12,4,3,2);
        ResponsiveColumn responsiveColumnSeven = new ResponsiveColumn(12,4,3,2);
        ResponsiveColumn responsiveColumnEight = new ResponsiveColumn(12,4,3,2);
        ResponsiveColumn responsiveColumnNine = new ResponsiveColumn(12,4,3,2);
        //
        responsiveColumnFive.withComponent(replacementTfToEncrypt_);
        //
        responsiveColumnSix.withComponent(replacementCharacterToReplace_);
        //
        responsiveColumnSeven.withComponent(replacementCharacterReplacement_);
        //
        responsiveColumnEight.withComponent(replacementEncryptButton_);
        //
        responsiveColumnNine.withComponent(replacementLabelEncryptedText_);
        //Set columns of responsiveRowReplacementEncryption
        responsiveRowReplacementEncryption.addColumn(responsiveColumnFive);
        responsiveRowReplacementEncryption.addColumn(responsiveColumnSix);
        responsiveRowReplacementEncryption.addColumn(responsiveColumnSeven);
        responsiveRowReplacementEncryption.addColumn(responsiveColumnEight);
        responsiveRowReplacementEncryption.addColumn(responsiveColumnNine);

        //Add Rows to Responsive Layout
        layout_.addRow(responsiveRowCaesarEncryption);
        layout_.addComponent(textLayoutReplacement_);
        layout_.addRow(responsiveRowReplacementEncryption);
        addComponents(textLayoutCaesar_, layout_);
        textLayoutCaesar_.setComponentAlignment(viewTitle_, Alignment.MIDDLE_CENTER);
        textLayoutCaesar_.setComponentAlignment(caesarDescription_, Alignment.MIDDLE_CENTER);
    }

    /**
     * Method for encrypting the text with caesar
     * @param event
     */
    private void caesarEncryptText(Button.ClickEvent event) {
        if (!caesarTfToEncypt_.getValue().equals(""))  {
            System.err.println(caesarTfToEncypt_.getValue());
            caesarLabelEncryptedText_.setValue(M114Constants.ENCRYPTED_TEXT + encryptionHelper_.caesarEncryption(caesarTfToEncypt_.getValue(), chkEncodeDecode_.getValue()));
        } else {
            Notification.show(M114Constants.ERROR_EMPTY_TEXTFIELD, Notification.Type.ERROR_MESSAGE);
        }
    }

    /**
     * Method for encrypting the text with Substitution
     * @param event
     */
    private void replacementEncryptText(Button.ClickEvent event) {
        if (replacementCharacterToReplace_.getValue().length() > 1 || replacementCharacterReplacement_.getValue().length() > 1) {
            Notification.show(M114Constants.ERROR_ONLY_ONE_CHARACTER, Notification.Type.ERROR_MESSAGE);
            return;
        }

        if (replacementCharacterToReplace_.getValue().equals("") || replacementCharacterReplacement_.getValue().equals("")) {
            Notification.show(M114Constants.ERRPR_NO_CHARACTERS, Notification.Type.ERROR_MESSAGE);
            return;
        }
        if (!replacementTfToEncrypt_.getValue().equals("")) {
            replacementLabelEncryptedText_.setValue(M114Constants.ENCRYPTED_TEXT + encryptionHelper_.replacementEncryption(replacementTfToEncrypt_.getValue(), replacementCharacterToReplace_.getValue(), replacementCharacterReplacement_.getValue()));
        } else {
            Notification.show(M114Constants.ERROR_EMPTY_TEXTFIELD, Notification.Type.ERROR_MESSAGE);
        }
    }
}
