package com.danilojakob.bs.symmetric.tests.caesar;

import com.danilojakob.bs.util.helper.EncryptionHelper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Class for testing the Caesar decoding
 */
public class CaesarDecodingTest {

    private EncryptionHelper encryptionHelper_;

    @Before
    public void createEncryptionHelperObject() {
        encryptionHelper_ = new EncryptionHelper();
    }

    @Test
    public void testCaesarDecoding() {
        String decryptedText = encryptionHelper_.caesarEncryption("Hermps", false);
        Assert.assertEquals("Danilo", decryptedText);
    }

    @After
    public void deleteEncryptionHelperObject() {
        encryptionHelper_ = null;
    }
}
