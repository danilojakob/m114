package com.danilojakob;

public interface M114Constants {

    //Menu Title
    String MENU_TITLE = "M114";

    //CDIView names
    String CDIVIEW_HOME = "home";
    String CDIVIEW_SYMMETRIC = "symmetric";
    String CDIVIEW_ASYMMETRIC = "asymmetric";
    String CDIVIEW_DOWNLOAD = "download";
    String CDIVIEW_UPLOAD = "upload";

    //Menu Captions
    String MENU_CAPTION_HOME = "Home";
    String MENU_CAPTION_SYMMETRIC = "Symmetrisch";
    String MENU_CAPTION_ASYMMETRIC = "Asymmetrisch";
    String MENU_CAPTION_GENERATE_KEYPAIR = "Keys generieren";
    String MENU_CAPTION_DECRYPT_TEXT = "Text encrypten";

    //View Descriptions
    String HOME_DESCRIPTION = "Diese Website soll die Verschlüsselung erklären können";

    //SymmetricView Texts
    String TF_TO_ENCRYPT_CAPTION = "Zu verschlüssender Text";
    String BUTTON_ENCRYPT_CAPTION = "Encrypt";
    String CAESAR_DESCRIPTION = "Die Caesar Verschlüsselung ist eine sogenannte Transposition Verschlüsselung.\n Sie versetzt zeichen im Text um eine definierte Länge.";
    String CHECKBOX_CAPTION = "Verschlüsseln";
    String ENCRYPTED_TEXT = "Der Text lautet: ";
    String CAESAR_TITLE = "Caesar:";
    String REPLACEMENT_TITLE = "Substitution: ";
    String REPLACEMENT_DESCRIPTION = "Bei einer Substitution Verschlüsselung wird im Text ein bestimmtes Zeichen ersetzt.";
    String TF_TO_REPLACE_CAPTION = "Zeichen ersetzen..";
    String TF_REPLACEMENT_CAPTION = "Durch...";

    //AsymmetricView Texts
    String RSA_KEY_TITLE = "RSA Schlüssel: ";
    String RSA_KEY_DESCRIPTION = "RSA Schlüssel bestehen aus einen Private und Public Key, bei diesen wird der Public key zu verschlüsselung und der Private Key zu entschlüsselung verwendet. " +
            "Der Private Key, wie der Name sugeriert soll geheim gehlaten werden während der Public Key weitergegeben werden darf";
    String TF_LENGTH_CAPTION = "Länge";
    String TF_TO_DECRYPT_CAPTION = "Zu entschlüssender Text";
    //Error Messages
    String ERROR_EMPTY_TEXTFIELD = "Es muss ein Text zum verschlüsseln angegeben werden!";
    String ERROR_ONLY_ONE_CHARACTER = "Es darf nur 1 Buchstabe angegeben werden";
    String ERROR_EMPTY_LENGTH = "Es muss eine Länge angegeben werden";
    String ERROR_KEY_TOO_SHORT = "Der Key muss mindestens 512 Lang sein";
    String ERROR_KEY_TOO_BIG = "Der Key darf nicht länger als 16384 sein";
    String ERROR_NOT_NUMBER = "Es muss eine Nummer sein";
    String ERRPR_NO_CHARACTERS = "Es muss ein Zeichen zum ersetzen und ein Zeichen als Ersatz angegeben werden";

    //Other
    String EMPTY_STRING = "";
    String WORKING_DIR_WORK = "C:\\Users\\JADA\\Documents\\Applikationen\\Vaadin\\M114\\m114\\";
    String WORKING_DIR_HOME = "F:\\Dokumente\\Applikationen\\Vaadin\\M114\\m114";
    String WORKING_DIR = "C:\\Users\\JADA\\Documents\\Applikationen\\Vaadin\\M114\\m114\\";
}
