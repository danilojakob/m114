package com.danilojakob.bs.asymmetric.boundary;

import com.danilojakob.M114Constants;
import com.danilojakob.bs.util.helper.DownloadHelper;
import com.danilojakob.bs.util.helper.EncryptionHelper;
import com.github.appreciated.app.layout.annotations.MenuIcon;
import com.jarektoro.responsivelayout.ResponsiveColumn;
import com.jarektoro.responsivelayout.ResponsiveLayout;
import com.jarektoro.responsivelayout.ResponsiveRow;
import com.vaadin.cdi.CDIView;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.server.Responsive;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.simplefiledownloader.SimpleFileDownloader;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

@CDIView(M114Constants.CDIVIEW_UPLOAD)
@MenuIcon(VaadinIcons.UPLOAD)
public class AsymmetricKeyUpload extends VerticalLayout implements View, Upload.Receiver {

    private Upload uploadPublicKey_;
    private Upload uploadPrivateKey_;
    private Upload uploadFile_;
    private Upload uploadFile2_;
    private Button encryptFile_;
    private Button decryptFile_;
    private ResponsiveLayout layout_;
    private TextField tfTextToEncrypt_;
    private TextField tfTextToDecrypt_;
    private Label encryptedText_;
    private Label decryptedText_;
    private Button encryptText_;
    private Button decryptText_;
    //
    private File key_;
    private File toEncrypt_;
    /* Keys */
    private PublicKey publicKey_;
    private PrivateKey privateKey_;
    //
    private String privateKeyPath_;
    private String publicKeyPath_;
    private String filePath_;
    //
    private EncryptionHelper encryptionHelper_;
    private DownloadHelper downloadHelper_;
    private SimpleFileDownloader downloader_;
    //
    private boolean hasPrivateKey_;
    private boolean hasPublicKey_;
    private boolean hasFile_;

    private final Logger LOG = LoggerFactory.getLogger(AsymmetricKeyUpload.class);

    public AsymmetricKeyUpload() {

        uploadPublicKey_ = new Upload("Public Key", this::receiveUploadPublicKey);
        uploadPrivateKey_ = new Upload("Private Key", this::receiveUploadPrivateKey);
        uploadFile_ = new Upload("File", this::receiveUpload);
        uploadFile2_ = new Upload("File", this::receiveUpload);
        encryptFile_ = new Button("Verschlüsseln");
        encryptFile_.addClickListener(this::encryptFile);
        decryptFile_ = new Button("Entschlüsseln");
        decryptFile_.addClickListener(this::decryptFile);
        layout_ = new ResponsiveLayout();
        //
        tfTextToEncrypt_ = new TextField();
        tfTextToEncrypt_.setPlaceholder(M114Constants.TF_TO_ENCRYPT_CAPTION);
        tfTextToDecrypt_ = new TextField();
        tfTextToDecrypt_.setPlaceholder(M114Constants.TF_TO_DECRYPT_CAPTION);
        encryptedText_ = new Label();
        decryptedText_ = new Label();
        decryptText_ = new Button("Entschlüsseln");
        decryptText_.addClickListener(this::decryptText);
        encryptText_ = new Button("Verschlüsseln");
        encryptText_.addClickListener(this::encryptText);
        //
        encryptionHelper_ = new EncryptionHelper();
        downloadHelper_ = new DownloadHelper();
        downloader_ = new SimpleFileDownloader();

        ResponsiveRow responsiveRowFileEncryption = new ResponsiveRow();
        responsiveRowFileEncryption.setHorizontalSpacing(false);
        responsiveRowFileEncryption.setVerticalSpacing(true);
        ResponsiveRow responsiveRowFileDecryption = new ResponsiveRow();
        responsiveRowFileDecryption.setHorizontalSpacing(false);
        responsiveRowFileDecryption.setVerticalSpacing(true);
        //
        ResponsiveRow responsiveRowTextEncryption = new ResponsiveRow();
        responsiveRowTextEncryption.setHorizontalSpacing(false);
        responsiveRowTextEncryption.setVerticalSpacing(true);
        ResponsiveRow responsiveRowTextDecryption = new ResponsiveRow();
        responsiveRowTextDecryption.setHorizontalSpacing(false);
        responsiveRowTextDecryption.setVerticalSpacing(true);

        ResponsiveColumn responsiveColumnOne = new ResponsiveColumn(12, 4, 3, 2);
        responsiveColumnOne.withComponent(uploadPublicKey_);
        ResponsiveColumn responsiveColumnTwo = new ResponsiveColumn(12, 4, 3, 2);
        responsiveColumnTwo.withComponent(uploadFile_);
        ResponsiveColumn responsiveColumnThree = new ResponsiveColumn(12, 4, 3, 2);
        responsiveColumnThree.withComponent(encryptFile_);
        //
        ResponsiveColumn responsiveColumnFour = new ResponsiveColumn(12, 4, 3, 2);
        responsiveColumnFour.withComponent(uploadPrivateKey_);
        ResponsiveColumn responsiveColumnFive = new ResponsiveColumn(12, 4, 3, 2);
        responsiveColumnFive.withComponent(uploadFile2_);
        ResponsiveColumn responsiveColumnSix = new ResponsiveColumn(12, 4, 3, 2);
        responsiveColumnSix.withComponent(decryptFile_);
        //
        responsiveRowFileEncryption.addColumn(responsiveColumnOne);
        responsiveRowFileEncryption.addColumn(responsiveColumnTwo);
        responsiveRowFileEncryption.addColumn(responsiveColumnThree);
        //
        responsiveRowFileDecryption.addColumn(responsiveColumnFour);
        responsiveRowFileDecryption.addColumn(responsiveColumnFive);
        responsiveRowFileDecryption.addColumn(responsiveColumnSix);
        /* */
        ResponsiveColumn responsiveColumnSeven = new ResponsiveColumn(12, 4, 3, 2);
        responsiveColumnSeven.withComponent(tfTextToEncrypt_);
        ResponsiveColumn responsiveColumnEight = new ResponsiveColumn(12 ,4, 3, 2);
        responsiveColumnEight.withComponent(encryptText_);
        ResponsiveColumn responsiveColumnNine = new ResponsiveColumn(12, 4, 3, 2);
        responsiveColumnNine.withComponent(encryptedText_);
        //
        ResponsiveColumn responsiveColumnTen = new ResponsiveColumn(12, 4, 3, 2);
        responsiveColumnTen.withComponent(tfTextToDecrypt_);
        ResponsiveColumn responsiveColumnEleven = new ResponsiveColumn(12, 4,3, 2);
        responsiveColumnEleven.withComponent(decryptText_);
        ResponsiveColumn responsiveColumnTwelve = new ResponsiveColumn(12, 4 , 3, 2);
        responsiveColumnTwelve.withComponent(decryptedText_);

        Responsive.makeResponsive(decryptedText_, encryptedText_);
        decryptedText_.setStyleName(ValoTheme.LAYOUT_HORIZONTAL_WRAPPING);
        decryptedText_.setWidth("100%");
        encryptedText_.setStyleName(ValoTheme.LAYOUT_HORIZONTAL_WRAPPING);
        encryptedText_.setWidth("100%");
        //
        responsiveRowTextEncryption.addColumn(responsiveColumnSeven);
        responsiveRowTextEncryption.addColumn(responsiveColumnEight);
        responsiveRowTextEncryption.addColumn(responsiveColumnNine);
        responsiveRowTextDecryption.addColumn(responsiveColumnTen);
        responsiveRowTextDecryption.addColumn(responsiveColumnEleven);
        responsiveRowTextDecryption.addColumn(responsiveColumnTwelve);

        layout_.addRow(responsiveRowFileEncryption);
        layout_.addRow(responsiveRowFileDecryption);
        layout_.addRow(responsiveRowTextEncryption);
        layout_.addRow(responsiveRowTextDecryption);

        addExtension(downloader_);

        addComponents(layout_);
    }

    /**
     * Method for receiving files
     * @param s {@link String} Filename
     * @param s1 {@link String} MimeType
     * @return
     */
    @Override
    public OutputStream receiveUpload(String s, String s1) {
        toEncrypt_ = new File(M114Constants.WORKING_DIR + s);
        filePath_ = M114Constants.WORKING_DIR + toEncrypt_.getName();
        hasFile_ = true;
        try {
            return new FileOutputStream(toEncrypt_);
        } catch (Exception ex) {
            LOG.error("Error while receiving file to encrypt / decrypt: {}", ex.toString() );
            hasFile_ = false;
        }
        return null;
    }

    /**
     * Receive the uploaded file
     * @param s {@link String} filename
     * @param s1 {@link String} mimeType
     * @return
     */
    private OutputStream receiveUploadPrivateKey(String s, String s1) {
        try {
            key_ = new File(M114Constants.WORKING_DIR + s);
            LOG.info("File Saved successfully: {}", key_.exists());
            hasPrivateKey_ = true;
            return new FileOutputStream(key_);
        } catch (IOException ex) {
            LOG.error("Error while receiving Private Key {})", ex.toString());
            hasPrivateKey_ = false;
            return null;
        }
    }

    /**
     * Receive the uploaded file
     * @param s {@link String} filename
     * @param s1 {@link String} mimeType
     * @return
     */
    private OutputStream receiveUploadPublicKey(String s, String s1) {
        try {
            key_ = new File(M114Constants.WORKING_DIR + s);
            hasPublicKey_ = true;
            return new FileOutputStream(key_);
        } catch (Exception ex) {
            LOG.error("Error while receiving Public Key");
            hasPublicKey_ = false;
        }
        return null;
    }

    /**
     * Method for decoding the encoded key file
     * @param file {@link File} file that needs to be decoded
     * @param isPrivateKey
     * @return
     */
    private Key decodeKey(File file, boolean isPrivateKey) {
        try {
            FileInputStream fis = new FileInputStream(file);
            DataInputStream dis = new DataInputStream(fis);
            byte[] encodedBytes = new byte[(int) file.length()];
            dis.readFully(encodedBytes);
            dis.close();
            fis.close();
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            if (isPrivateKey) {
                PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(encodedBytes);
                return keyFactory.generatePrivate(spec);
            } else {
                X509EncodedKeySpec spec = new X509EncodedKeySpec(encodedBytes);
                return keyFactory.generatePublic(spec);
            }
        } catch (Exception ex) {
            LOG.debug("Exception while decoding the file: {}", ex.toString());
        }
        return null;
    }

    /**
     * Method for encrypting the file
     * @param event
     */
    private void encryptFile(Button.ClickEvent event) {
        publicKey_ = (PublicKey) decodeKey(key_, false);
        publicKeyPath_ = M114Constants.WORKING_DIR + key_.getName();
        if (hasPublicKey_ && hasFile_ && toEncrypt_.exists()) {
            encryptionHelper_.encryptFile(toEncrypt_, publicKey_);
            try {
                downloader_.setFileDownloadResource(downloadHelper_.createStreamResource(M114Constants.WORKING_DIR_WORK + toEncrypt_.getName(), "encryptedFile." + FilenameUtils.getExtension(toEncrypt_.getName())));
                downloader_.download();
                clearFiles();
            } catch (IOException ex) {
                LOG.info("Error while downloading file: {}", ex.toString());
            }
        } else {
            if (!toEncrypt_.exists() || !hasPublicKey_ || !hasPrivateKey_) {
                Notification.show("Die Files sind noch nicht vollständig hochgeladen", Notification.Type.ERROR_MESSAGE);
            }
            if (!hasFile_ && hasPublicKey_) {
                Notification.show("Es muss noch ein File zum verschlüsseln angegeben werden", Notification.Type.ERROR_MESSAGE);
            } else if (!hasPublicKey_ && hasFile_) {
                Notification.show("Es muss noch der Public Key angegeben werden", Notification.Type.ERROR_MESSAGE);
            } else if (!hasPublicKey_ && !hasFile_) {
                Notification.show("Es muss ein File und Public Key angegeben werden", Notification.Type.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Method for decrypting the file
     * @param event
     */
    private void decryptFile(Button.ClickEvent event) {
        privateKey_ = (PrivateKey) decodeKey(key_, true);
        privateKeyPath_ = M114Constants.WORKING_DIR + key_.getName();
        if (hasPrivateKey_ && hasFile_ && toEncrypt_.exists()) {
            encryptionHelper_.decryptFile(toEncrypt_, privateKey_);
            try {
                downloader_.setFileDownloadResource(downloadHelper_.createStreamResource(M114Constants.WORKING_DIR_WORK + toEncrypt_.getName(), "decryptedFile." + FilenameUtils.getExtension(toEncrypt_.getName())));
                downloader_.download();
                clearFiles();
            } catch (IOException ex) {
                LOG.info("Error while downloading file: {}", ex.toString());
            }
        } else {
            if (!toEncrypt_.exists() || !key_.exists()) {
                Notification.show("Die Files sind noch nicht vollständig hochgeladen", Notification.Type.ERROR_MESSAGE);
            }
            if (!hasFile_ && !hasPrivateKey_) {
                Notification.show("Es muss noch ein File zum verschlüsseln angegeben werden", Notification.Type.ERROR_MESSAGE);
            } else if (!hasPrivateKey_ && hasFile_) {
                Notification.show("Es muss noch der Private Key angegeben werden", Notification.Type.ERROR_MESSAGE);
            } else if (!hasPrivateKey_ && !hasFile_) {
                Notification.show("Es muss ein File und Priva   te Key angegeben werden", Notification.Type.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Method for encrypting text
     * @param event
     */
    private void encryptText(Button.ClickEvent event) {
        try {
            publicKey_ = (PublicKey) decodeKey(key_, false);
            publicKeyPath_ = M114Constants.WORKING_DIR + key_.getName();
            encryptedText_.setValue("Text: " + encryptionHelper_.encryptText(tfTextToEncrypt_.getValue(), publicKey_));
            clearFiles();
        } catch (Exception ex) {
            LOG.error("Error while encrypting Text: {}", ex.toString());
            clearFiles();
        }
    }

    /**
     * Method for decrypting text
     * @param event
     */
    private void decryptText(Button.ClickEvent event) {
        try {
            privateKey_ = (PrivateKey) decodeKey(key_, true);
            privateKeyPath_ = M114Constants.WORKING_DIR + key_.getName();
            decryptedText_.setValue("Text: " + encryptionHelper_.decryptText(tfTextToDecrypt_.getValue(), privateKey_));
            clearFiles();
        } catch (Exception ex) {
            LOG.error("Error while decrypting Text: {}", ex.toString());
            clearFiles();
        }
    }

    /**
     * Method for cleaning client files from server
     */
    private void clearFiles() {
        try {
            if (privateKeyPath_ != null && new File(privateKeyPath_).exists()) {
                Files.delete(Paths.get(privateKeyPath_));
            }
            if (publicKeyPath_ != null && new File(publicKeyPath_).exists()) {
                Files.delete(Paths.get(publicKeyPath_));
            }
            if (filePath_ != null && new File(filePath_).exists()) {
                Files.delete(Paths.get(filePath_));
            }
        } catch (IOException ex) {
            LOG.error("Error while clearing files: {}", ex.toString());
        }
    }
}
