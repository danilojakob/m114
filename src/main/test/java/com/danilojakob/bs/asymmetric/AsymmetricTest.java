package com.danilojakob.bs.asymmetric;

import com.danilojakob.bs.asymmetric.tests.rsa.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses( {
        GenerateKeyPairTest.class,
        SaveKeyPairTest.class,
        //EncryptTextTest.class,
        DecryptTextTest.class,
        EncryptFileTest.class,
        DecryptFileTest.class
})
public class AsymmetricTest {
}
