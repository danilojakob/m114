package com.danilojakob.bs.home.boundary;

import com.danilojakob.M114Constants;
import com.github.appreciated.app.layout.annotations.MenuIcon;
import com.vaadin.cdi.CDIView;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@CDIView(M114Constants.CDIVIEW_HOME)
@MenuIcon(VaadinIcons.HOME)
public class HomeView extends VerticalLayout implements View {

    private Label viewTitle_;
    private Label description_;

    private final Logger LOG = LoggerFactory.getLogger(HomeView.class);
    public HomeView() {
        LOG.info("Entered HomeView");
        //Create objects in constructor due to injection chain problems
        viewTitle_ = new Label(M114Constants.MENU_CAPTION_HOME);
        viewTitle_.addStyleName(ValoTheme.LABEL_H1);
        description_ = new Label(M114Constants.HOME_DESCRIPTION);
        description_.setStyleName(ValoTheme.LABEL_H2);
        description_.setStyleName(ValoTheme.LAYOUT_HORIZONTAL_WRAPPING);
        description_.setWidth("100%");
        Responsive.makeResponsive(description_);

        addComponents(viewTitle_, description_);
        setComponentAlignment(viewTitle_, Alignment.MIDDLE_CENTER);
        setComponentAlignment(description_, Alignment.MIDDLE_CENTER);
    }
}
