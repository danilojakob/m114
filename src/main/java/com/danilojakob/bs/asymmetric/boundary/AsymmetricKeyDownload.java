package com.danilojakob.bs.asymmetric.boundary;

import com.danilojakob.M114Constants;
import com.danilojakob.bs.util.helper.DownloadHelper;
import com.danilojakob.bs.util.helper.KeyGenerator;
import com.github.appreciated.app.layout.annotations.MenuIcon;
import com.jarektoro.responsivelayout.ResponsiveColumn;
import com.jarektoro.responsivelayout.ResponsiveLayout;
import com.jarektoro.responsivelayout.ResponsiveRow;
import com.vaadin.cdi.CDIView;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.server.Responsive;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.simplefiledownloader.SimpleFileDownloader;

@CDIView(M114Constants.CDIVIEW_DOWNLOAD)
@MenuIcon(VaadinIcons.DOWNLOAD)
public class AsymmetricKeyDownload extends VerticalLayout implements View {

    private Button downloadPrivateButton_;
    private Button downloadPublicButton_;
    private SimpleFileDownloader downloader_;
    private DownloadHelper downloadHelper_;
    private Label viewTitle_;
    private Label keyTitle_;
    private Label keyDescription_;
    private TextField tfLength_;
    private Button generateKeysButton_;
    private ResponsiveLayout layout_;
    private VerticalLayout rsaTextLayout_;

    //Logger
    private final Logger logger = LoggerFactory.getLogger(AsymmetricKeyDownload.class);

    private KeyGenerator keyGenerator_;

    public AsymmetricKeyDownload() {
        logger.info("Entered");
        downloader_ = new SimpleFileDownloader();
        downloadHelper_ = new DownloadHelper();
        downloadPrivateButton_ = new Button("Download Private Key");
        downloadPrivateButton_.addClickListener(this::download);
        downloadPrivateButton_.setVisible(false);
        downloadPublicButton_ = new Button("Download Public Key");
        downloadPublicButton_.addClickListener(this::download);
        downloadPublicButton_.setVisible(false);
        layout_ = new ResponsiveLayout();
        viewTitle_ = new Label(M114Constants.MENU_CAPTION_GENERATE_KEYPAIR);
        viewTitle_.addStyleName(ValoTheme.LABEL_H1);
        keyTitle_ = new Label(M114Constants.RSA_KEY_TITLE);
        keyTitle_.setStyleName(ValoTheme.LABEL_H2);
        keyTitle_.setStyleName(ValoTheme.LAYOUT_HORIZONTAL_WRAPPING);
        keyTitle_.setWidth("100%");
        keyDescription_ = new Label(M114Constants.RSA_KEY_DESCRIPTION);
        keyDescription_.setStyleName(ValoTheme.LABEL_H3);
        keyDescription_.setStyleName(ValoTheme.LAYOUT_HORIZONTAL_WRAPPING);
        keyDescription_.setWidth("100%");
        rsaTextLayout_ = new VerticalLayout();
        rsaTextLayout_.setMargin(false);
        rsaTextLayout_.addComponents(viewTitle_, keyTitle_, keyDescription_);
        rsaTextLayout_.setComponentAlignment(viewTitle_, Alignment.MIDDLE_CENTER);
        //
        tfLength_ = new TextField();
        tfLength_.setPlaceholder(M114Constants.TF_LENGTH_CAPTION);
        generateKeysButton_ = new Button("Generate Keys");
        generateKeysButton_.addClickListener(this::generateKeys);
        ResponsiveRow responsiveRowOne = new ResponsiveRow();
        ResponsiveColumn responsiveColumnOne = new ResponsiveColumn(12, 4, 3, 2);
        responsiveColumnOne.withComponent(tfLength_);
        ResponsiveColumn responsiveColumnTwo = new ResponsiveColumn(12, 4, 3, 2);
        responsiveColumnTwo.withComponent(generateKeysButton_);
        ResponsiveColumn responsiveColumnThree = new ResponsiveColumn(12, 4, 3, 2);
        responsiveColumnThree.withComponent(downloadPrivateButton_);
        ResponsiveColumn responsiveColumnFour = new ResponsiveColumn(12, 4, 3 ,2);
        responsiveColumnFour.withComponent(downloadPublicButton_);
        //
        responsiveRowOne.setVerticalSpacing(true);
        responsiveRowOne.setHorizontalSpacing(false);
        responsiveRowOne.addColumn(responsiveColumnOne);
        responsiveRowOne.addColumn(responsiveColumnTwo);
        responsiveRowOne.addColumn(responsiveColumnThree);
        responsiveRowOne.addColumn(responsiveColumnFour);
        layout_.addRow(responsiveRowOne);
        Responsive.makeResponsive(keyTitle_, keyDescription_);


        addComponents(rsaTextLayout_, layout_);
        addExtension(downloader_);
    }

    /**
     * Method for downloading the keys
     * @param event
     */
    private void download(Button.ClickEvent event) {
        try {
            //File Path to the Keys
            String filePath = event.getButton().getCaption().equals("Download Public Key") ? "keyPairs\\publicKey" : "keyPairs\\privateKey";
            //Save as private or public key
            String type = event.getButton().getCaption().equals("Download Public Key") ? "publickey.pem" : "privatekey.pem";
            StreamResource streamResource = downloadHelper_.createStreamResource(M114Constants.WORKING_DIR + filePath, type);
            downloader_.setFileDownloadResource(streamResource);
            downloader_.download();
        } catch (Exception ex) {
        }
    }

    /**
     * Method for generating the keys
     * @param event
     */
    private void generateKeys(Button.ClickEvent event) {
        //Check if textfield is empty
        if (tfLength_.getValue() == null || tfLength_.getValue().equals("")) {
            Notification.show(M114Constants.ERROR_EMPTY_LENGTH, Notification.Type.ERROR_MESSAGE);
            return;
        }

        //Check if value of textfield is a number
        try {
            Integer.valueOf(tfLength_.getValue());
        } catch (Exception ex) {
            Notification.show(M114Constants.ERROR_NOT_NUMBER, Notification.Type.ERROR_MESSAGE);
            return;
        }

        //Check if number is smaller than 512
        if (Integer.valueOf(tfLength_.getValue()) < 512) {
            Notification.show(M114Constants.ERROR_KEY_TOO_SHORT, Notification.Type.ERROR_MESSAGE);
            return;
        }

        //Check if number is bigger than 16384
        if (Integer.valueOf(tfLength_.getValue()) > 16384) {
            Notification.show(M114Constants.ERROR_KEY_TOO_BIG, Notification.Type.ERROR_MESSAGE);
            return;
        }

        try {
            keyGenerator_ = new KeyGenerator(Integer.valueOf(tfLength_.getValue()));
        } catch (Exception ex) {
            System.err.println(ex);
        }
        downloadPrivateButton_.setVisible(true);
        downloadPublicButton_.setVisible(true);
        keyGenerator_.generateKeys();
        try {
            keyGenerator_.saveKeys(M114Constants.WORKING_DIR + "keyPairs\\publicKey", keyGenerator_.getPublicKey().getEncoded());
            keyGenerator_.saveKeys(M114Constants.WORKING_DIR + "keyPairs\\privateKey", keyGenerator_.getPrivateKey().getEncoded());
            Notification.show("Keys wurden generiert", Notification.Type.HUMANIZED_MESSAGE);
        } catch (Exception ex) {
            System.err.println(ex);
        }

    }
}
