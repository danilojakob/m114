package com.danilojakob.bs.ui;

import javax.inject.Inject;

import com.danilojakob.M114Constants;
import com.danilojakob.bs.asymmetric.boundary.AsymmetricKeyDownload;
import com.danilojakob.bs.asymmetric.boundary.AsymmetricKeyUpload;
import com.danilojakob.bs.asymmetric.boundary.AsymmetricView;
import com.danilojakob.bs.home.boundary.HomeView;
import com.danilojakob.bs.symmetric.boundary.SymmetricView;
import com.github.appreciated.app.layout.AppLayout;
import com.github.appreciated.app.layout.behaviour.AppLayoutComponent;
import com.github.appreciated.app.layout.behaviour.Behaviour;
import com.github.appreciated.app.layout.builder.Section;
import com.github.appreciated.app.layout.builder.design.AppLayoutDesign;
import com.github.appreciated.app.layout.builder.elements.builders.SubmenuBuilder;
import com.github.appreciated.app.layout.builder.factories.DefaultVaadinCdiNavigationElementInfoProducer;
import com.github.appreciated.app.layout.component.MenuHeader;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.Viewport;
import com.vaadin.cdi.CDIUI;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.PushStateNavigation;
import com.vaadin.navigator.ViewProvider;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

/**
 * Class for the Menu Layout
 */

@CDIUI("")
@Theme("mytheme")
@Viewport("width=device-width, initial-scale=1")
@Title("M114")
@PushStateNavigation
public class MyUI extends UI {

    private VerticalLayout content_;

    private AppLayoutComponent drawer_;
    @Inject
    private ViewProvider viewProvider_;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        content_ = new VerticalLayout();
        content_.setMargin(false);
        buildMenu(Behaviour.LEFT_RESPONSIVE_HYBRID_NO_APP_BAR);
        setContent(content_);
        content_.setSizeFull();
    }

    /**
     * Method for building the Menu with AppLayout
     * @param behaviour {@link Behaviour} type of AppLayout
     */
    private void buildMenu(Behaviour behaviour) {
        content_.removeAllComponents();
        //Creates the Menu
        drawer_ = AppLayout.getCDIBuilder(behaviour)
                .withViewProvider(() -> viewProvider_)
                .withNavigationElementInfoProducer(new DefaultVaadinCdiNavigationElementInfoProducer())
                .withTitle(M114Constants.MENU_TITLE)
                .withScrollToTopOnNavigate(true)
                .withDesign(AppLayoutDesign.MATERIAL)
                .add(new MenuHeader("M114", new ThemeResource("logo.png")), Section.HEADER)
                //Menu Items
                .add(M114Constants.MENU_CAPTION_HOME, HomeView.class)
                .add(M114Constants.MENU_CAPTION_SYMMETRIC, SymmetricView.class)
                .add(SubmenuBuilder.get("Asymmetrisch", VaadinIcons.LOCK)
                        .add(M114Constants.MENU_CAPTION_GENERATE_KEYPAIR, AsymmetricKeyDownload.class)
                        .add(M114Constants.MENU_CAPTION_DECRYPT_TEXT, AsymmetricKeyUpload.class)
                        .build()
                )
                .build();
        drawer_.addStyleName("left");
        content_.addComponent(drawer_);
        //By default needs to navigate to home
        if (UI.getCurrent().getNavigator() != null) {
            UI.getCurrent().getNavigator().navigateTo("home");
        }
    }
}
