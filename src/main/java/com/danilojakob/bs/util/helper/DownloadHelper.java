package com.danilojakob.bs.util.helper;

import com.vaadin.server.StreamResource;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

public class DownloadHelper {

    private final Logger LOG = LoggerFactory.getLogger(DownloadHelper.class);

    /**
     * Method to create a Vaadin StreamResource out of file
     * @param filePath {@link String} Path to the File
     * @param filename {@link String} Name of the File
     * @return {@link StreamResource}
     * @throws IOException
     */
    public StreamResource createStreamResource(String filePath, String filename) throws IOException {
        File file = new File(filePath);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(file));
        LOG.info("Stream Resource created successfully");
        return new StreamResource((StreamResource.StreamSource) () -> inputStream, filename);
    }
}
