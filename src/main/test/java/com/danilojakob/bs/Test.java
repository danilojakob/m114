package com.danilojakob.bs;

import com.danilojakob.bs.asymmetric.AsymmetricTest;
import com.danilojakob.bs.symmetric.SymmetricTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        SymmetricTest.class,
        AsymmetricTest.class
})
public class Test {
}
